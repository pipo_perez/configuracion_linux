#Configuración Linux.

#Configuración alias personalizada.
	# some more ls aliases
	alias ls="lsd -l"
	alias ll='ls -alF'
	alias la='ls -A'
	alias l='ls -CF'
	alias off="poweroff"
	alias c="clear"
	alias x="chmod +x"
	alias rr="rm -rf"
	#Alias de cat a bat
	alias cat="bat"
	alias catn="/bin/cat"
	alias catnl="/bin/bat --paging=never"
	#Fin de la conf del bat
	
#Guia de instalación bat
	1° Buscamos repositorio de bat en github.

		- Url: https://github.com/sharkdp/bat/releases/tag/v0.22.1
		- Descargamos el archivo con la extención .deb
		- dpkg -i "nombre del archivo"
		
	2° Ponemos los alias que tenemos arriba para dejarlo configurados.
	
	
#Funciones para el archivo de configuración (valido para bash y zsh)

function work(){
        mkdir -p work/{nmap,documentos,script}
}


function extractPorts(){
        ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')"
        ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)"
        echo -e "\n[*] Extracting information...\n" > extractPorts.tmp
        echo -e "\t[*] IP Address: $ip_address"  >> extractPorts.tmp
        echo -e "\t[*] Open ports: $ports\n"  >> extractPorts.tmp
        echo $ports | tr -d '\n' | xclip -sel clip
        echo -e "[*] Ports copied to clipboard\n"  >> extractPorts.tmp
        cat extractPorts.tmp; rm extractPorts.tmp
}
